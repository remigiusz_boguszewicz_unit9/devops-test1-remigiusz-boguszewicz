#!/bin/bash

V_ENVIRONMENT=$1

if [ -z "$V_ENVIRONMENT" ]; then
  echo "[error] Please provite environment to deploy to as first parameter. Exiting. "
  exit 1
fi


./index.sh > test/index.html
./status.sh > test/status.html
./s_login.sh

echo "[info] Running surge on $V_ENVIRONMENT"
case $V_ENVIRONMENT in
  "remek-dev.surge.sh")
    surge --project /opt/atlassian/pipelines/agent/build/test/ --domain $V_ENVIRONMENT
    ;;
  "remek-client.surge.sh")
    surge --project /opt/atlassian/pipelines/agent/build/test/ --domain $V_ENVIRONMENT
    ;;
  "remek-prod.surge.sh")
    surge --project /opt/atlassian/pipelines/agent/build/test/ --domain $V_ENVIRONMENT
    ;;
  "remek-feature.surge.sh")
    echo "[info] checking if proper tag is in place for this kind of build"
    echo "BITBUCKET_TAG: $BITBUCKET_TAG"
    V_TAG_CHECK=`echo $BITBUCKET_TAG | grep feature | wc -l`
    if [ "$V_TAG_CHECK" -gt 0 ]; then
      surge --project /opt/atlassian/pipelines/agent/build/test/ --domain $V_ENVIRONMENT
    else
      echo "[error] Appropriate Tag was not set. Aborting deployment. Exiting."
      exit 1
    fi
    ;;
  *)
    echo "Unknown environment!!! Exiting."
    exit 1
    ;;
esac

