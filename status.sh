#!/bin/bash

cat test/header.txt

echo "Env status"
echo "<table border=0 width=100%>"
echo "<tr><td>Development</td><td>Master(Client)</td><td>Production</td><td>Feature</td></tr>"


cat << EOF > /tmp/status.txt
http://remek-dev.surge.sh/index.html
http://remek-client.surge.sh/index.html
http://remek-prod.surge.sh/index.html
http://remek-feature.surge.sh/index.html
EOF

echo "<tr>"
while read LINE
do
  echo "<td>"
  echo "<a href=$LINE> $LINE</a>"
  echo "</td>"
done < /tmp/status.txt

echo "<tr>"
while read LINE
do
  echo "<td>"
  echo "<iframe src=\"$LINE\" seamless width=350 height=500></iframe>"
  echo "</td>"
done < /tmp/status.txt


echo "</td>"
echo "</tr>"
echo "</table>"

cat test/footer.txt
