#!/bin/bash

git pull
while read LINE
do
  LINE1=`echo $LINE | awk '{print $1}'`
  LINE2=`echo $LINE | awk '{print $2}'`
  echo "Adding $LINE2"
  echo "<img src=\"$LINE1\" alt=\"$LINE2\" width=\"350\">" > test/content.txt
  git add test/content.txt
  git commit -m "pic: $LINE2"
  git push
done < pictures.txt

