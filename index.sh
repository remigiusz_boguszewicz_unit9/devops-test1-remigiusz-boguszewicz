#!/bin/bash

cat test/header.txt

F_TMP=/tmp/show_env.$$.txt
env | grep -v PASS | grep BITBUCKET | grep -v BITBUCKET_CLONE_DIR | grep -v BITBUCKET_REPO_UUID | grep -v BITBUCKET_REPO_OWNER | grep -v BITBUCKET_REPO_SLUG | grep -v BITBUCKET_REPO_OWNER_UUID > $F_TMP

# adding some meta information to display
echo "created=`date '+%Y-%m-%d--%H:%M:%S'`" >> $F_TMP
#GIT_COUNT=`git rev-list --count HEAD`
#echo "GIT_REVISION_COUNT=$GIT_COUNT" >> $F_TMP

echo "<table border=1>"

while read LINE
do
  LINE1=`echo $LINE | awk -F"=" '{print $1}'`
  LINE2=`echo $LINE | awk -F"=" '{print $2}'`
  echo "<tr><td>$LINE1</td><td>$LINE2</td></tr>"
done < $F_TMP

echo "</table>"
rm -f $F_TMP

cat test/content.txt

cat test/footer.txt
